from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import index, ambilData

# Create your tests here.

class story8UnitTest(TestCase):

    def test_ada_landingpage(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_apakah_menggunakan_fungsi_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
        
    def test_apakah_ada_search_box(self):
        response = Client().get('')
        self.assertContains(response, "Cari buku disini")
        self.assertEqual(response.status_code, 200)

