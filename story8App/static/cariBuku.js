$(document).ready(()=>{
  $.ajax({
    method:'GET',
    url :'ambilData?key=' + 'batman',
    success:function(response){

      let bookShelf = response.items
      $('#badanTableBuku').empty();

      for(let i=0;i<response.items.length;i++) {
        let book = bookShelf[i].volumeInfo
        var name = $('<td>').text(book.title);

        if ('authors' in book == false) var authors = $('<td>').text("-");
        else var authors = $('<td>').text(book.authors);

        if ('imageLinks' in book == false)
          var img = $('<td>').text("-");
        else {
          if ('smallThumbnail' in book.imageLinks == false)
            var img = $('<td>').append($('<img>').attr({
              'src': book.imageLinks.thumbnail
            }));
          else
            var img = $('<td>').append($('<img>').attr({
              'src': book.imageLinks.smallThumbnail
            }));
        }

      var tr = $('<tr>').append(name, authors, img);

      $('#badanTableBuku').append(tr);

      }   
      }
      
  })
  $('#btnSubmit').click(function(){
      var key= $('#kolomSearchBuku').val();

      $.ajax({
        method:'GET',
        url :'ambilData?key=' + key,
        success:function(response){

          let bookShelf = response.items
          $('#badanTableBuku').empty();

          for(let i=0;i<response.items.length;i++) {
            let book = bookShelf[i].volumeInfo
            var name = $('<td>').text(book.title);

            if ('authors' in book == false) var authors = $('<td>').text("-");
            else var authors = $('<td>').text(book.authors);

            if ('imageLinks' in book == false)
              var img = $('<td>').text("-");
            else {
              if ('smallThumbnail' in book.imageLinks == false)
                var img = $('<td>').append($('<img>').attr({
                  'src': book.imageLinks.thumbnail
                }));
              else
                var img = $('<td>').append($('<img>').attr({
                  'src': book.imageLinks.smallThumbnail
                }));
            }

          var tr = $('<tr>').append(name, authors, img);

          $('#badanTableBuku').append(tr);

          }   
          }
          
      })
  })

  $('#kolomSearchBuku').keypress(function (e) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == 13){
      var key= $('#kolomSearchBuku').val();

      $.ajax({
        method:'GET',
        url :'ambilData?key=' + key,
        success:function(response){

          let bookShelf = response.items
          $('#badanTableBuku').empty();

          for(let i=0;i<response.items.length;i++) {
            let book = bookShelf[i].volumeInfo
            var name = $('<td>').text(book.title);

            if ('authors' in book == false) var authors = $('<td>').text("-");
            else var authors = $('<td>').text(book.authors);

            if ('imageLinks' in book == false)
              var img = $('<td>').text("-");
            else {
              if ('smallThumbnail' in book.imageLinks == false)
                var img = $('<td>').append($('<img>').attr({
                  'src': book.imageLinks.thumbnail
                }));
              else
                var img = $('<td>').append($('<img>').attr({
                  'src': book.imageLinks.smallThumbnail
                }));
            }

          var tr = $('<tr>').append(name, authors, img);

          $('#badanTableBuku').append(tr);

          }

              
          }
          
      })
     }
   }); 
})
